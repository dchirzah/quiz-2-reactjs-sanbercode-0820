class BangunDatar {
  constructor(nama) {
    this._nama = nama;
  }
  get nama() {
    return this._nama;
  }
  set name(x) {
    return (this.nama = x);
  }
  luas() {
    return ("");
  }
  keliling() {
    return ("");
  }
}
var bentuk = new BangunDatar("bangun datar");

console.log(bentuk.nama);
console.log(bentuk.luas());
console.log(bentuk.keliling());

class Lingkaran extends BangunDatar {
  constructor(nama, r) {
    super(nama);
    this.jari = r;
  }
  luas() {
    console.log ("Luas dari lingkaran jari-jari " +this.jari +" adalah "+ (22/7)*this.jari);
  }
  keliling() {
    console.log ("Keliling dari lingkaran jari-jari "+this.jari +" adalah "+ (2*(22/7)*this.jari));
  }
}

class Persegi extends BangunDatar {
  constructor(nama, sisi) {
    super(nama);
    this.sisi = sisi;
  }
  luas() {
    console.log("Luas dari persegi sisi " +this.sisi +" adalah "+this.sisi*this.sisi)
    return;
  }
  keliling() {
    console.log("Keliling dari persegi sisi "+this.sisi +"adalah "+ (4*this.sisi)) ;
  }
}

var lingkaran = new Lingkaran("lingkaran",7);
lingkaran.nama;
lingkaran.r;
lingkaran.luas();
lingkaran.keliling();


var persegi = new Persegi("kotak",10);
persegi.nama;
persegi.sisi;
persegi.luas();
persegi.keliling();


