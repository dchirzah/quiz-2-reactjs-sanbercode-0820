let warna = ["biru", "merah", "kuning", "hijau"];

let dataBukuTambahan = {
    penulis: "john doe",
    tahunTerbit : 2020
}

let buku = {
    nama: "pemrograman dasar",
    jumlahHalaman: 172,
    warnaSampul : ["hitam"]
}

function tambah() {
    buku.warnaSampul.push([...warna]);
    return console.log (buku.warnaSampul)
}


console.log(tambah());